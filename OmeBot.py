from selenium import webdriver 
from selenium.webdriver.support.ui import WebDriverWait 
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.common.keys import Keys 
from selenium.webdriver.common.by import By 
from selenium.webdriver.chrome.options import Options
import time
import winsound

class Bot:
    def __init__(self):
        op = Options()
        op.add_argument('disable-infobars')
        self.driver = webdriver.Chrome('D:/Apps/Chrome Driver/chromedriver.exe', chrome_options=op)

        self.driver.get("https://www.omegle.com/") 
        field = self.driver.find_element_by_class_name('newtopicinput')
        field.send_keys("Music")
        field.send_keys(Keys.ENTER)

        text = self.driver.find_element_by_id('chattypetextcell')
        text.click()

        self.chat = False
        self.t = 0

    def banner(self):
        while True:
            time.sleep(.25)
            try:
                field = self.driver.find_element_by_class_name('chatmsg ')
                field.send_keys("I'm just a guy who wanna talk about Music")
                field.send_keys(Keys.ENTER)
                break
            except:
                pass                
        print("banner printed!")

    def alive(self):
        time.sleep(0.25)
        try:
            logs = self.driver.find_elements_by_class_name('statuslog')
            for log in logs:
                if log.text == 'Stranger has disconnected.' or log.text == 'You have disconnected.':
                    return False
        except:
            pass
        return True

    def validate(self):
        print("\nvalidating... ")
        self.banner()
        self.t = time.time()
        while(time.time() - self.t < 15):
            if not self.alive():
                print("Douche!", int(time.time() - self.t))
                time.sleep(1)
                self.driver.find_element_by_class_name('disconnectbtn').click()
                return False
        print("That's the one!!")
        winsound.Beep(2500,500)
        return True

    def next(self):
        while not self.validate():
            pass
        while True:
            k = self.alive()
            time.sleep(1)
            if not k:
                print("That went well!", int(time.time() - self.t))
                time.sleep(5)
                self.driver.find_element_by_class_name('disconnectbtn').click()
                return

    def process(self):
        while True:
            self.next()
                


b = Bot()
b.process()
    
